import sys

from PyQt5.QtCore import QLocale, QTranslator, QLibraryInfo
from PyQt5.QtWidgets import QApplication
from pywinsparkle import pywinsparkle

import ui.MainWindow


def shutdown():
    sys.exit()


if __name__ == '__main__':
    pywinsparkle.win_sparkle_set_shutdown_request_callback(shutdown)

    update_url = "http://vps393437.ovh.net/update/appcast.xml"
    pywinsparkle.win_sparkle_set_appcast_url(update_url)
    pywinsparkle.win_sparkle_set_app_details("PSIG", "ActivitatsApp", '0.5')
    pywinsparkle.win_sparkle_init()

    pywinsparkle.win_sparkle_check_update_without_ui()

    app = QApplication(sys.argv)

    locale = QLocale(QLocale.Catalan, QLocale.Spain)
    QLocale.setDefault(locale)

    translator = QTranslator()
    translator.load(locale, 'qt', '_', QLibraryInfo.location(QLibraryInfo.TranslationsPath))
    app.installTranslator(translator)

    baseTranslator = QTranslator()
    baseTranslator.load('qtbase_', locale.name(), QLibraryInfo.location(QLibraryInfo.TranslationsPath))
    app.installTranslator(baseTranslator)

    window = ui.MainWindow.MainWindow()
    window.show()

    code = app.exec_()
    pywinsparkle.win_sparkle_cleanup()
    sys.exit()
