# -*- mode: python -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=[r'C:\Users\Abel Toy\AppData\Local\Programs\Python\Python35\Lib\site-packages\PyQt5\Qt\bin', r'M:\PSIG\Projectes\CTBB\Activitats'],
             binaries=[(r'C:\Users\Abel Toy\AppData\Local\Programs\Python\Python35\Lib\site-packages\pywinsparkle\libs\x64', '.')],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='ActivitatsApp',
          debug=False,
          strip=False,
          upx=True,
          console=False,
          icon=r'resources\icon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='ActivitatsApp')
