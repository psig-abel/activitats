# coding: utf-8
import operator

from PyQt5.QtCore import QAbstractTableModel, Qt
from sqlalchemy import Boolean, Column, Date, ForeignKey, Integer, Numeric, String, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()
metadata = Base.metadata


class Capca(Base):
    __tablename__ = 'TA_CAPCA'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Codi_CAPCA', Integer, primary_key=True)
    activitat = Column('ACTIVITAT', String(255))
    id_comentari_activitat = Column('Comentari_ACTIVITAT', Integer)
    codi = Column('CODI_CAPCA', String(255))
    grup = Column('GRUP', String(255))
    id_comentari_grup = Column('Comentari_GRUP', Integer)
    codi_nivell1 = Column('Codi_1rNivell', String(255))
    codi_nivell1i2 = Column('Codi_1ri2nNivell', ForeignKey('activitats.TA_CAPCA_1ri2nNivell.CODI_CAPCA_1ri2nNivell'))

    nivell1i2 = relationship('CapcaNivell1i2')


class CapcaNivell1(Base):
    __tablename__ = 'TA_CAPCA_1rNivell'
    __table_args__ = {'schema': 'activitats'}

    codi_nivell1 = Column('CODI_CAPCA_1rNivell', String(255), primary_key=True, unique=True)
    activitat_1r = Column('ACTIVITAT_1rN', String(255))


class CapcaNivell1i2(Base):
    __tablename__ = 'TA_CAPCA_1ri2nNivell'
    __table_args__ = {'schema': 'activitats'}

    codi_nivell1 = Column('CODI_CAPCA_1rNivell', ForeignKey('activitats.TA_CAPCA_1rNivell.CODI_CAPCA_1rNivell'),
                          nullable=False)
    codi_nivell1i2 = Column('CODI_CAPCA_1ri2nNivell', String(255), primary_key=True, unique=True)
    activitat_2n = Column('ACTIVITAT_2nN', String(255))

    nivell1 = relationship('CapcaNivell1')


class Carrerer(Base):
    __tablename__ = 'TA_Carrerer'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Carrerer', String(4), primary_key=True)
    fid = Column('FID', Integer)
    carrer = Column('carrer', String(255))
    tipus_via = Column('tipus_via', String(255))
    via = Column('via', String(255))
    enllac = Column('enllac', String(255))
    etiqueta = Column('etiqueta', String(255))
    name = Column('name', String(255))
    tipus = Column('tipus', Integer)
    nucli = Column('nucli', String(255))
    ine = Column('ine', Integer)


class LpcaaGrup(Base):
    __tablename__ = 'TA_LPCAA_Grup'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Grup_LPCAA', Integer, primary_key=True)
    codi = Column('Codi_Grup_LPCAA', String(255))
    descripcio = Column('Descripció', String(255))


class LpcaaRegimIntervencio(Base):
    __tablename__ = 'TA_LPCAA_Regim_Intervencio'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Annex_LPCAA', Integer, primary_key=True)
    annex = Column('Annex_LPCAA', String(255))
    descripcio = Column('Descripció', String(255))


class LpcaaCodiActivitat(Base):
    __tablename__ = 'TA_LPCAA_codi_activitats'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Codi_Activitat_LPCAA', Integer, primary_key=True)
    id_annex = Column('Id_Annex_LPCAA', ForeignKey('activitats.TA_LPCAA_Regim_Intervencio.Id_Annex_LPCAA'))
    id_grup = Column('Id_Grup_LPCAA', ForeignKey('activitats.TA_LPCAA_Grup.Id_Grup_LPCAA'))
    codi_activitat = Column('Codi_activitat_LPCAA', String(255))
    descripcio = Column('Descripció', String)

    regim_intervencio = relationship('LpcaaRegimIntervencio')
    grup = relationship('LpcaaGrup')


class Nucli(Base):
    __tablename__ = 'TA_NUCLIS'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id', Integer, nullable=False)
    nucli = Column('NUCLI', String(255))
    codi = Column('codi', String(255), primary_key=True, unique=True)


class Repar(Base):
    __tablename__ = 'TA_REPAR'
    __table_args__ = {'schema': 'activitats'}

    id_repar = Column('Id_REPAR', Integer, primary_key=True)
    id_tipus_ubicacio = Column('Id_tipus_ubicacio',
                               ForeignKey('activitats.TA_REPAR_Tipus_ubicacio.Id_tipus_ubicacio', ondelete='CASCADE',
                                          onupdate='CASCADE'), nullable=False)
    id_aforament = Column('Id_Aforament',
                          ForeignKey('activitats.TA_REPAR_Tipus_Aforament.Id_Aforament', ondelete='CASCADE',
                                     onupdate='CASCADE'), nullable=False)

    aforament = relationship('ReparTipusAforament')
    tipus_ubicacio = relationship('ReparTipusUbicacio')


class ReparSubtipusAcces(Base):
    __tablename__ = 'TA_REPAR_Sub_Tipus_Ubicacio_acces'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Sub_Tipus_Acess', Integer, primary_key=True)
    subtipus = Column('Sub_tipus', String(60))
    descripcio = Column('Descripcio', String(255))


class ReparSubtipusPerm(Base):
    __tablename__ = 'TA_REPAR_Sub_Tipus_Ubicacio_perm'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_sub_tipus_perm', Integer, primary_key=True)
    subtipus = Column('Sub_tipus', String(60))
    descripcio = Column('Descripcio', String(255))


class ReparTipusAforament(Base):
    __tablename__ = 'TA_REPAR_Tipus_Aforament'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Aforament', Integer, primary_key=True)
    aforament = Column('Aforament', String(10))
    descripcio = Column('Descripcio', String(25))


class ReparTipusGeneric(Base):
    __tablename__ = 'TA_REPAR_Tipus_Generic_Ubicacio'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Tipus_Generic', Integer, primary_key=True)
    tipus = Column('Tipus_generic', String(50))


class ReparTipusUbicacio(Base):
    __tablename__ = 'TA_REPAR_Tipus_ubicacio'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_tipus_ubicacio', Integer, primary_key=True)
    id_tipus_generic = Column('Id_Tipus_Generic_Ubicacio',
                              ForeignKey('activitats.TA_REPAR_Tipus_Generic_Ubicacio.Id_Tipus_Generic',
                                         ondelete='CASCADE', onupdate='CASCADE'), nullable=False)
    id_subtipus_perm = Column('Id_Sub_Tipus_Ubicacio_perm',
                              ForeignKey('activitats.TA_REPAR_Sub_Tipus_Ubicacio_perm.Id_sub_tipus_perm',
                                         ondelete='CASCADE', onupdate='CASCADE'), nullable=False)
    id_subtipus_acces = Column('Id_Sub_Tipus_Ubicacio_Acces',
                               ForeignKey('activitats.TA_REPAR_Sub_Tipus_Ubicacio_acces.Id_Sub_Tipus_Acess',
                                          ondelete='CASCADE', onupdate='CASCADE'), nullable=False)

    subtipus_acces = relationship('ReparSubtipusAcces')
    subtipus_perm = relationship('ReparSubtipusPerm')
    tipus_generic = relationship('ReparTipusGeneric')


class RegimIntervencio(Base):
    __tablename__ = 'TA_Regim_Intervencio'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Regim_Intervencio', Integer, primary_key=True)
    regim = Column('Regim_Intervencio', String(150))
    ambit = Column('Ambit_Intervencio', String(255))


class RegimIntervencioAntic(Base):
    __tablename__ = 'TA_Regim_Intervencio_Antic'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Regim_Intervencio_concessio', Integer, primary_key=True)
    regim = Column('Regim_Intervencio', String(150))
    normativa = Column('Normativa', String(150))


class Ubicacio(Base):
    __tablename__ = 'TA_Ubicacio'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Ubicacio', Integer, primary_key=True)
    id_carrerer = Column('Id_Carrerer', ForeignKey('activitats.TA_Carrerer.Id_Carrerer', onupdate='CASCADE'),
                         nullable=False)
    numero = Column('numero', String(25))
    utm_x = Column('utm_x', String(255))
    utm_y = Column('utm_y', String(255))
    codi_cadastral = Column('Codi_Cadastral', String(20))
    nucli = Column('Nucli', String(255))
    link_sig = Column('Link_SIG', String(255))

    carrerer = relationship('Carrerer')


class Activitat(Base):
    __tablename__ = 'activitats_co'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Activitat', Integer, primary_key=True,
                server_default=text("nextval('activitats.activitats_id_seq'::regclass)"))
    id_ubicacio = Column('Id_Ubicacio', ForeignKey('activitats.TA_Ubicacio.Id_Ubicacio'), nullable=False)
    activa = Column('En_actiu?', Boolean)
    nom_empresa = Column('Nom_empresa', String(75))
    cif_empresa = Column('CIFEmpresa', String(9))
    rao_social = Column('Rao_social', String(75))
    noms_anteriors = Column('Noms_anteriors', String(255))
    nom_titular = Column('Titular', String(75))
    sexe_titular = Column('Sexe del Titular', String(4))
    nif_titular = Column('NIFTitular', String(9))
    activitat = Column('Activitat', String(255))
    foto_path = Column('RutaFoto', String(255))
    tel_empresa1 = Column('Tel_empresa1', String(14))
    tel_empresa2 = Column('Tel_empresa2', String(50))
    fax_empresa = Column('Fax_empresa', String(14))
    web_empresa = Column('Web_empresa', String(255))
    email_corporatiu = Column('email_corporatiu', String(50))
    nom_contacte = Column('Contacte', String(50))
    tel_contacte = Column('Tel_contacte', String(14))
    mob_contacte = Column('Mob_contacte', String(14))
    email_contacte = Column('email_contacte', String(50))
    via_efeces_notificacions = Column('Via_efeces_notificacions', String(50))
    num_efectes_notificacions = Column('Nr_efectes notificacions', String(15))
    població_efectes_notificacons = Column('Població_efectes_notificacons', String(50))
    direccio_ppal = Column('direccio_ppal', Boolean)
    superficie_comercial = Column('superficie_comercial', Numeric(10, 2))

    ubicacio = relationship('Ubicacio')
    st = relationship('ActivitatSt', uselist=False, back_populates='activitat')


class ActivitatSt(Base):
    __tablename__ = 'activitats_st'
    __table_args__ = {'schema': 'activitats'}

    id = Column('Id_Activitat',
                ForeignKey('activitats.activitats_co.Id_Activitat', ondelete='CASCADE', onupdate='CASCADE'),
                primary_key=True)
    ref_expedient_principal = Column('ST_Ref_expedient_principal', String(20))
    id_regim_intervencio_concessio = Column('ST_Id_Regim_Intervencio_concessio', ForeignKey(
        'activitats.TA_Regim_Intervencio_Antic.Id_Regim_Intervencio_concessio', ondelete='SET NULL',
        onupdate='CASCADE'))
    id_regim_intervencio_actual = Column('ST_Id_Regim_Intervencio_actual',
                                         ForeignKey('activitats.TA_Regim_Intervencio.Id_Regim_Intervencio',
                                                    ondelete='SET NULL', onupdate='CASCADE'))
    data_concessio_comunicacio = Column('ST_Data_concessio_comunicacio', Date)
    data_control_inicial = Column('ST_Data_control_inicial', Date)
    caducitat_regim_intervencio = Column('ST_Caducitat_Regim_Intervenció', Date)
    cessament_activitat = Column('ST_Cessament_Activitat', Date)
    regim_disciplinari = Column('ST_Regim_disciplinari', String(255))
    eri_notes = Column('ST_ERiNotes', String(255))
    id_codi_activitat_lpcaa = Column('ST_Id_Codi_Activitat_LPCAA',
                                     ForeignKey('activitats.TA_LPCAA_codi_activitats.Id_Codi_Activitat_LPCAA',
                                                ondelete='SET NULL', onupdate='CASCADE'))
    data_darrera_revisio_lpcaa = Column('ST_Data_darrera_revisió_LPCAA', Date)
    data_darrer_cont_periodic_lpcaa = Column('ST_Data_darrer_cont_periodic_LPCAA', Date)
    data_proper_control_periódic_lpcaa = Column('ST_Data_proper_control_periódic_LPCAA', Date)
    permis_abocament_amb = Column('ST_Permis_abocament_AMB', Boolean)
    data_permis_abocament_amb = Column('ST_Data_Permis_abocament_AMB', Date)
    data_caducitat_permis_amb = Column('ST_Data_caducitat_permis_AMB', Date)
    incidencies_aigues_residuals = Column('ST_Incidencies_Aigues_Residuals', String(255))
    te_capca = Column('ST_CAPCA', Boolean)
    id_codi_capca = Column('ST_Id_Codi_CAPCA',
                           ForeignKey('activitats.TA_CAPCA.Id_Codi_CAPCA', ondelete='SET NULL', onupdate='CASCADE'))
    incidencies_cont_atm = Column('ST_Incidencies_Cont_Atm', String(255))
    te_repar = Column('ST_REPAR', Boolean)
    id_repar = Column('ST_Id_REPAR',
                      ForeignKey('activitats.TA_REPAR.Id_REPAR', ondelete='SET NULL', onupdate='CASCADE'))
    repar_aforament_maxim = Column('ST_REPAR_Aforament_maxim', Integer)
    incendis_com_prev = Column('ST_Incendis_Com_Prev', Boolean)
    urban_comp_urbanistica = Column('ST_URBAN_Comp_urbanistica', Boolean)
    nom_contacte = Column('ST_Contacte', String(50))
    tel_contacte = Column('ST_Tel_contacte', String(14))
    mob_contacte = Column('ST_Mob_contacte', String(14))
    email_contacte = Column('ST_email_contacte', String(50))

    lpcaa = relationship('LpcaaCodiActivitat')
    capca = relationship('Capca')
    repar = relationship('Repar')
    regim_intervencio_actual = relationship('RegimIntervencio')
    regim_intervencio_concessio = relationship('RegimIntervencioAntic')

    activitat = relationship("Activitat", back_populates='st')


class AlchemicalTableModelColumn:
    def __init__(self, header, sort_field, getter=None, boolean=False):
        self.header = header
        self.sort_field = sort_field
        self.getter = getter
        self.boolean = boolean


class AlchemicalTableModel(QAbstractTableModel):
    def __init__(self, query, columns):
        super().__init__()
        self.fields = columns
        self.query = query

        self.results = None
        self.count = None
        self.sorting = None
        self._filter = None

        self.loaded = False

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.fields[col].header
        return None

    def refresh(self):
        self.layoutAboutToBeChanged.emit()

        q = self.query
        if self.sorting is not None:
            order, col = self.sorting
            col = self.fields[col].sort_field
            if order == Qt.DescendingOrder:
                col = col.desc()
        else:
            col = None

        if self._filter is not None:
            q = q.filter(self._filter)

        if self.sorting is not None:
            q = q.order_by(col)

        self.results = q.all()
        self.count = len(self.results)
        self.layoutChanged.emit()
        self.loaded = True

    def flags(self, index):
        if not index.isValid():
            return None
        flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
        if self.fields[index.column()].boolean:
            flags |= Qt.ItemIsUserCheckable
        return flags

    def supportedDropActions(self):
        return Qt.MoveAction

    def dropMimeData(self, data, action, row, col, parent):
        if action != Qt.MoveAction:
            return
        return False

    def rowCount(self, parent=None):
        return self.count or 0

    def columnCount(self, parent=None):
        return len(self.fields)

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role not in (Qt.DisplayRole, Qt.EditRole, Qt.CheckStateRole):
            return None
        row = self.results[index.row()]
        name = self.fields[index.column()].getter
        data = operator.attrgetter(name)(row)

        if self.fields[index.column()].boolean:
            if role == Qt.DisplayRole:
                return None
            elif role == Qt.CheckStateRole:
                return Qt.Checked if data else Qt.Unchecked
        elif role == Qt.CheckStateRole:
            return None

        return data

    def setData(self, index, value, role=None):
        return False

    def model(self, index):
        if not self.loaded:
            self.refresh()
        return self.results[index]

    def sort(self, col, order):
        self.sorting = order, col
        self.refresh()

    @property
    def filter(self):
        return self._filter

    @filter.setter
    def filter(self, value):
        self._filter = value
        self.refresh()
