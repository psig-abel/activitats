; -- Languages.iss --
; Demonstrates a multilingual installation.

; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName={cm:MyAppName}
AppId=ActivitatsApp
AppVersion=0.5
DefaultDirName={userappdata}\{cm:MyAppName}
DefaultGroupName={cm:MyAppName}
UninstallDisplayIcon={app}\ActivitatsApp.exe
VersionInfoDescription={cm:MyAppName} Setup
VersionInfoProductName={cm:MyAppName}
OutputDir=.
PrivilegesRequired=lowest
; Uncomment the following line to disable the "Select Setup Language"
; dialog and have it rely solely on auto-detection.
;ShowLanguageDialog=no
; If you want all languages to be listed in the "Select Setup Language"
; dialog, even those that can't be displayed in the active code page,
; uncomment the following line. Note: Unicode Inno Setup always displays
; all languages.
;ShowUndisplayableLanguages=yes
SolidCompression=True
Compression=lzma2/ultra64

[Languages]
Name: ca; MessagesFile: "compiler:Languages\Catalan.isl"

[CustomMessages]
ca.MyDescription=Gestor de la base de dades d'activitats
ca.MyAppName=ActivitatsApp
ca.MyAppVerName=ActivitatsApp %1
ca.CreateStartMenuIcon=Crea una icona al men� d'inici

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"

[Icons]
Name: "{userdesktop}\{cm:MyAppName}"; Filename: "{app}\ActivitatsApp.exe"; IconFilename: "{app}\icon.ico"; Tasks: desktopicon
Name: "{group}\{cm:MyAppName}"; Filename: "{app}\ActivitatsApp.exe"; IconFilename: "{app}\icon.ico"

[Files]
Source: "..\..\dist\ActivitatsApp\_bz2.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\_ctypes.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\_decimal.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\_hashlib.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\_lzma.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\_multiprocessing.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\_socket.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\_sqlite3.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\_ssl.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\_win32sysloader.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\ActivitatsApp.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\ActivitatsApp.exe.manifest"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\base_library.zip"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\mfc100u.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\Microsoft.VC90.CRT.manifest"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\msvcm90.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\msvcp90.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\MSVCP140.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\msvcr90.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\MSVCR100.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\psycopg2._psycopg.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\pyexpat.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\PyQt5.Qt.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\PyQt5.QtCore.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\PyQt5.QtGui.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\PyQt5.QtPrintSupport.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\PyQt5.QtWidgets.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\python3.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\python35.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\pythoncom35.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\pywintypes35.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\Qt5Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\Qt5Gui.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\Qt5PrintSupport.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\Qt5Svg.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\Qt5Widgets.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\select.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\sip.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\sqlite3.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\unicodedata.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\VCRUNTIME140.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\win32api.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\win32com.shell.shell.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\win32trace.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\win32ui.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\win32wnet.pyd"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\WinSparkle.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\Include\pyconfig.h"; DestDir: "{app}\Include\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\lib2to3\Grammar.txt"; DestDir: "{app}\lib2to3\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\lib2to3\Grammar3.5.3.final.0.pickle"; DestDir: "{app}\lib2to3\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\lib2to3\PatternGrammar.txt"; DestDir: "{app}\lib2to3\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\lib2to3\PatternGrammar3.5.3.final.0.pickle"; DestDir: "{app}\lib2to3\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\lib2to3\tests\data\README"; DestDir: "{app}\lib2to3\tests\data\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\Include\pyconfig.h"; DestDir: "{app}\Include\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\iconengines\qsvgicon.dll"; DestDir: "{app}\qt5_plugins\iconengines\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\imageformats\qgif.dll"; DestDir: "{app}\qt5_plugins\imageformats\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\imageformats\qicns.dll"; DestDir: "{app}\qt5_plugins\imageformats\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\imageformats\qico.dll"; DestDir: "{app}\qt5_plugins\imageformats\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\imageformats\qjpeg.dll"; DestDir: "{app}\qt5_plugins\imageformats\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\imageformats\qsvg.dll"; DestDir: "{app}\qt5_plugins\imageformats\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\imageformats\qtga.dll"; DestDir: "{app}\qt5_plugins\imageformats\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\imageformats\qtiff.dll"; DestDir: "{app}\qt5_plugins\imageformats\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\imageformats\qwbmp.dll"; DestDir: "{app}\qt5_plugins\imageformats\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\imageformats\qwebp.dll"; DestDir: "{app}\qt5_plugins\imageformats\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\platforms\qminimal.dll"; DestDir: "{app}\qt5_plugins\platforms\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\platforms\qoffscreen.dll"; DestDir: "{app}\qt5_plugins\platforms\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\platforms\qwindows.dll"; DestDir: "{app}\qt5_plugins\platforms\"; Flags: ignoreversion
Source: "..\..\dist\ActivitatsApp\qt5_plugins\printsupport\windowsprintersupport.dll"; DestDir: "{app}\qt5_plugins\printsupport\"; Flags: ignoreversion
Source: "..\icon.ico"; DestDir: "{app}"; Flags: ignoreversion

[Run]
Filename: "{app}\ActivitatsApp.exe"; WorkingDir: "{app}"; Flags: postinstall

[Dirs]
Name: "{app}\lib2to3\"
Name: "{app}\lib2to3\tests\"
Name: "{app}\lib2to3\tests\data\"
Name: "{app}\Include\"
Name: "{app}\qt5_plugins\"
Name: "{app}\qt5_plugins\iconengines\"
Name: "{app}\qt5_plugins\imageformats\"
Name: "{app}\qt5_plugins\platforms\"
Name: "{app}\qt5_plugins\printsupport\"
