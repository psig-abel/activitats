from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.orm import sessionmaker, load_only

import models

engine = create_engine(URL('postgres', query={'service': 'gis_ctbb'}))
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()

results = session.query(models.Ubicacio).options(load_only(models.Ubicacio.codi_cadastral)).filter(
    models.Ubicacio.codi_cadastral.isnot(None)).order_by(models.Ubicacio.codi_cadastral)
for result in results:
    print(result.codi_cadastral)
