from PyQt5.QtWidgets import QWidget

from ui.compiled.Ui_ImpressioInactives import Ui_ImpressioInactives


class ImpressioInactives(QWidget, Ui_ImpressioInactives):
    def __init__(self, parent=None):
        super(ImpressioInactives, self).__init__(parent)
        self.setupUi(self)
