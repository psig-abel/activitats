from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QMessageBox

from ui.compiled.Ui_Imprimeix import Ui_Imprimeix


class Imprimeix(QDialog, Ui_Imprimeix):
    def __init__(self, parent, subtitle, model, widget_type):
        super(Imprimeix, self).__init__(parent, flags=Qt.Window)

        self.setupUi(self)

        self.subtitol.setText('(' + subtitle + ')')

        self.model = model
        self.model.refresh()
        for i in range(0, model.rowCount()):
            widget = widget_type(self.llista)
            self.llista.layout().addWidget(widget)
            self.set_data(widget, i)

        self.printButton.clicked.connect(self.print_list)

    def set_data(self, widget, row):
        for i in range(0, self.model.columnCount()):
            field_name = self.model.headerData(i, Qt.Horizontal, Qt.DisplayRole)
            label = getattr(widget, field_name)
            label.setText(str(self.model.index(row, i).data() or ''))

    def print_list(self):
        QMessageBox.information(self, "Impressió no disponible", "La funcionalitat d'imprimir encara no està acabada…")
        # printer = QPrinter()
        #
        # dialog = QPrintDialog(printer, self)
        # dialog.setWindowTitle('Imprimeix llista')
        # if dialog.exec() != QDialog.Accepted:
        #     return
        # painter = QPainter()
        # painter.begin(printer)
        # self.llista.render(painter)
        # painter.end()
