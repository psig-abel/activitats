import logging

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMainWindow, QCheckBox, QRadioButton, QButtonGroup, QLineEdit, QComboBox, QCompleter, \
    QHBoxLayout
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.orm import sessionmaker, load_only, joinedload

import widgets
from ui.ImpressioInactives import ImpressioInactives
from ui.ImpressioRepar import ImpressioRepar
from ui.Imprimeix import Imprimeix
from models import AlchemicalTableModelColumn, AlchemicalTableModel, Activitat, Ubicacio, Carrerer, Nucli, ActivitatSt, \
    ReparTipusAforament, Repar, ReparTipusUbicacio, ReparTipusGeneric, ReparSubtipusAcces, ReparSubtipusPerm
from ui.RegistreGeneral import RegistreGeneral
from ui.compiled.Ui_MainWindow import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__(flags=Qt.Window)

        # TODO: Adreça poder escollir també número
        # TODO: Impressions
        # TODO: Impressions flow layout

        self.setupUi(self)

        logging.basicConfig()
        logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

        engine = create_engine(URL('postgres', query={'service': 'gis_ctbb'}))
        session_maker = sessionmaker()
        session_maker.configure(bind=engine)
        session = session_maker()

        query = session.query(Activitat).options(
            load_only(Activitat.nom_empresa, Activitat.activa),
            joinedload(Activitat.ubicacio).load_only(Ubicacio.numero).joinedload(
                Ubicacio.carrerer).load_only(Carrerer.etiqueta)
        ).join(Activitat.ubicacio).join(Ubicacio.carrerer).join(Activitat.st)

        self.model = AlchemicalTableModel(query, columns=[
            AlchemicalTableModelColumn('En actiu?', Activitat.activa, 'activa', {'boolean': True}),
            AlchemicalTableModelColumn('Nom de l\'empresa', Activitat.nom_empresa, 'nom_empresa'),
            AlchemicalTableModelColumn('Nom del carrer o via', Carrerer.via, 'ubicacio.carrerer.etiqueta'),
            AlchemicalTableModelColumn('Núm.', Ubicacio.numero, 'ubicacio.numero'),
        ])
        self.activitatsView.setModel(self.model)
        self.activitatsView.sortByColumn(1, Qt.AscendingOrder)
        self.activitatsView.resizeColumnsToContents()

        flow_layout = widgets.FlowLayout()
        self.filtraGroupBox.toggled.connect(self.on_filter_enable)
        self.filtraGroupBox.layout().addLayout(flow_layout)
        self.filter_layout = QHBoxLayout()
        self.show_inactive = False
        self.mostra_actives = QCheckBox()
        self.mostra_actives.setText("Mostra inactives")
        self.mostra_actives.clicked.connect(self.on_show_inactive)
        self.filter_layout.addWidget(self.mostra_actives)
        self.filtraGroupBox.layout().addLayout(self.filter_layout)

        self.filter_options = [
            FilterOption(
                'Referència cadastral',
                AlchemicalTableModel(
                    session.query(Ubicacio).options(load_only(Ubicacio.codi_cadastral)).filter(
                        Ubicacio.codi_cadastral.isnot(None)).order_by(Ubicacio.codi_cadastral),
                    [AlchemicalTableModelColumn('Codi cadastral', Ubicacio.codi_cadastral, 'codi_cadastral')]
                ),
                lambda x: Ubicacio.codi_cadastral == x.codi_cadastral
            ),
            FilterOption(
                'Adreça',
                AlchemicalTableModel(
                    session.query(Carrerer).options(load_only(Carrerer.name)).order_by(Carrerer.name),
                    [AlchemicalTableModelColumn('Carrer', Carrerer.name, 'name')]
                ),
                lambda x: Carrerer.name == x.name
            ),
            FilterOption(
                'Nucli',
                AlchemicalTableModel(
                    session.query(Nucli).options(load_only(Nucli.codi, Nucli.nucli)).order_by(Nucli.nucli),
                    [AlchemicalTableModelColumn('Nucli', Nucli.nucli, 'nucli')]
                ),
                lambda x: Ubicacio.nucli == x.codi
            ),
            FilterOption(
                'Nom empresa',
                AlchemicalTableModel(
                    session.query(Activitat).options(load_only(Activitat.nom_empresa)).filter(
                        Activitat.nom_empresa.isnot(None)).distinct(Activitat.nom_empresa).order_by(
                        Activitat.nom_empresa),
                    [AlchemicalTableModelColumn('Nom empresa', Activitat.nom_empresa, 'nom_empresa')]
                ),
                lambda x: Activitat.nom_empresa.like('%' + x.nom_empresa + '%')
            ),
            FilterOption(
                'Nom titular',
                AlchemicalTableModel(
                    session.query(Activitat).options(load_only(Activitat.nom_titular)).filter(
                        Activitat.nom_titular.isnot(None)).distinct(Activitat.nom_titular).order_by(
                        Activitat.nom_titular),
                    [AlchemicalTableModelColumn('Nom titular', Activitat.nom_titular, 'nom_titular')]
                ),
                lambda x: Activitat.nom_titular.like('%' + x.nom_titular + '%')
            ),
            FilterOption(
                'Nº expedient',
                AlchemicalTableModel(
                    session.query(ActivitatSt).options(load_only(ActivitatSt.ref_expedient_principal)).filter(
                        ActivitatSt.ref_expedient_principal.isnot(None)).distinct(
                        ActivitatSt.ref_expedient_principal).order_by(ActivitatSt.ref_expedient_principal),
                    [AlchemicalTableModelColumn('Nº expedient', ActivitatSt.ref_expedient_principal,
                                                'ref_expedient_principal')]
                ),
                lambda x: ActivitatSt.ref_expedient_principal == x.ref_expedient_principal
            ),
            FilterOption('Règim d\'intervenció', None, None),
            FilterOption(
                'ID activitat',
                AlchemicalTableModel(
                    session.query(Activitat).options(load_only(Activitat.id)).order_by(Activitat.id),
                    [AlchemicalTableModelColumn('ID', Activitat.id, 'id')]
                ),
                lambda x: Activitat.id == x.id
            )
        ]
        self.filter_option = self.filter_options[0]
        self.filter_control = None

        button_group = QButtonGroup(self.filtraGroupBox)
        for i, filter_option in enumerate(self.filter_options):
            radio = QRadioButton()
            radio.setText(filter_option.name)
            button_group.addButton(radio, i)
            if i == 0:
                radio.setChecked(True)
            flow_layout.addWidget(radio)
        button_group.buttonToggled[int, bool].connect(self.on_radio)

        self.addButton.clicked.connect(lambda: RegistreGeneral(self).show())

        self.printReparButton.clicked.connect(lambda: self.print_repar(session))
        self.printInactivesButton.clicked.connect(lambda: self.print_inactives(session))

        self.on_radio(0, True)
        self.on_filter_enable(False)

    def on_radio(self, id, checked):
        if not checked:
            return
        if self.filter_control is not None:
            if hasattr(self.filter_control, 'text'):
                self.filter_option.old_value = self.filter_control.text()
            elif hasattr(self.filter_control, 'currentText'):
                self.filter_option.old_value = self.filter_control.currentIndex()
            self.filter_layout.removeWidget(self.filter_control)
            self.filter_control.setParent(None)
            self.filter_control.deleteLater()
            self.filter_control = None
        self.filter_option = self.filter_options[id]
        if self.filter_option.model is None:
            self.filter_control = QLineEdit()
            if self.filter_option.old_value is not None:
                self.filter_control.setText(self.filter_option.old_value)
        else:
            self.filter_control = QComboBox()
            self.filter_control.setEditable(True)
            self.filter_control.setInsertPolicy(QComboBox.NoInsert)
            self.filter_control.setModel(self.filter_option.model)
            self.filter_control.completer().setCompletionMode(QCompleter.InlineCompletion)
            self.filter_control.currentIndexChanged.connect(self.on_filter)
            if self.filter_option.old_value is not None:
                self.filter_control.setCurrentIndex(self.filter_option.old_value)
        self.filter_layout.insertWidget(0, self.filter_control, 1)
        self.on_filter()

    def on_filter(self, _=None, active=True):
        if active:
            model_filter = self.filter_option.filter(self.filter_criteria())
        else:
            model_filter = None
        if not self.show_inactive:
            if model_filter is not None:
                model_filter = (Activitat.activa == True) & (model_filter)
            else:
                model_filter = (Activitat.activa == True)
        self.model.filter = model_filter

    def on_filter_enable(self, on):
        if on:
            self.show_inactive = self.mostra_actives.isChecked()
            if self.filter_control is not None:
                self.on_filter()
            else:
                self.on_radio(0, True)
        else:
            self.show_inactive = False
            self.on_filter(None, False)

    def on_show_inactive(self, on):
        self.show_inactive = on
        self.on_filter()

    def filter_criteria(self):
        if hasattr(self.filter_control, 'text'):
            return self.filter_control.text()
        elif hasattr(self.filter_control, 'currentIndex'):
            return self.filter_option.model.model(self.filter_control.currentIndex())
        return None

    def show_print_dialog(self, subtitle, model, widget):
        dialog = Imprimeix(self, subtitle, model, widget)
        dialog.show()

    def print_repar(self, session):
        reparModel = AlchemicalTableModel(
            session.query(Activitat).options(
                load_only(Activitat.nom_empresa, Activitat.nom_titular, Activitat.activitat, Activitat.sexe_titular,
                          Activitat.nif_titular),
                joinedload(Activitat.st).load_only(ActivitatSt.repar_aforament_maxim).joinedload(
                    ActivitatSt.repar).joinedload(Repar.aforament).load_only(
                    ReparTipusAforament.aforament, ReparTipusAforament.descripcio),
                joinedload(Activitat.st).joinedload(ActivitatSt.repar).joinedload(Repar.tipus_ubicacio).joinedload(
                    ReparTipusUbicacio.tipus_generic).load_only(ReparTipusGeneric.tipus),
                joinedload(Activitat.st).joinedload(ActivitatSt.repar).joinedload(Repar.tipus_ubicacio).joinedload(
                    ReparTipusUbicacio.subtipus_acces).load_only(ReparSubtipusAcces.subtipus,
                                                                 ReparSubtipusAcces.descripcio),
                joinedload(Activitat.st).joinedload(ActivitatSt.repar).joinedload(Repar.tipus_ubicacio).joinedload(
                    ReparTipusUbicacio.subtipus_perm).load_only(ReparSubtipusPerm.subtipus,
                                                                ReparSubtipusPerm.descripcio)
            ).join(Activitat.st).join(ActivitatSt.repar).join(Repar.aforament).join(Repar.tipus_ubicacio).join(
                ReparTipusUbicacio.tipus_generic).join(ReparTipusUbicacio.subtipus_acces).join(
                ReparTipusUbicacio.subtipus_perm).order_by(Activitat.nom_empresa).filter(ActivitatSt.te_repar == True),
            [AlchemicalTableModelColumn('nomEmpresa', Activitat.nom_empresa, 'nom_empresa'),
             AlchemicalTableModelColumn('titular', Activitat.nom_titular, 'nom_titular'),
             AlchemicalTableModelColumn('activitat', Activitat.activitat, 'activitat'),
             AlchemicalTableModelColumn('genere', Activitat.sexe_titular, 'sexe_titular'),
             AlchemicalTableModelColumn('nif', Activitat.nif_titular, 'nif_titular'),
             AlchemicalTableModelColumn('tipusAforament', ReparTipusAforament.aforament,
                                        'st.repar.aforament.aforament'),
             AlchemicalTableModelColumn('descripcioAforament', ReparTipusAforament.descripcio,
                                        'st.repar.aforament.descripcio'),
             AlchemicalTableModelColumn('aforament', ActivitatSt.repar_aforament_maxim, 'st.repar_aforament_maxim'),
             AlchemicalTableModelColumn('tipusGeneric', ReparTipusGeneric.tipus,
                                        'st.repar.tipus_ubicacio.tipus_generic.tipus'),
             AlchemicalTableModelColumn('tipusAcces', ReparSubtipusAcces.subtipus,
                                        'st.repar.tipus_ubicacio.subtipus_acces.subtipus'),
             AlchemicalTableModelColumn('descripcioAcces', ReparSubtipusAcces.descripcio,
                                        'st.repar.tipus_ubicacio.subtipus_acces.descripcio'),
             AlchemicalTableModelColumn('tipusDurada', ReparSubtipusPerm.subtipus,
                                        'st.repar.tipus_ubicacio.subtipus_perm.subtipus'),
             AlchemicalTableModelColumn('descripcioDurada', ReparSubtipusPerm.descripcio,
                                        'st.repar.tipus_ubicacio.subtipus_perm.descripcio')
             ]
        )
        self.show_print_dialog(
            'Segons els articlea 85 i 87 del Reglament d\'espectacles públics i activitats recreatives (Decret 112/2010)',
            reparModel, ImpressioRepar)

    def print_inactives(self, session):
        inactivesModel = AlchemicalTableModel(
            session.query(Activitat).options(
                load_only(Activitat.nom_empresa, Activitat.nom_titular, Activitat.activitat),
                joinedload(Activitat.ubicacio).load_only(Ubicacio.numero, Ubicacio.nucli).joinedload(
                    Ubicacio.carrerer).load_only(Carrerer.etiqueta)).join(Activitat.ubicacio).join(
                Ubicacio.carrerer).order_by(Activitat.nom_empresa).filter(Activitat.activa == False),
            [AlchemicalTableModelColumn('nomEmpresa', Activitat.nom_empresa, 'nom_empresa'),
             AlchemicalTableModelColumn('titular', Activitat.nom_titular, 'nom_titular'),
             AlchemicalTableModelColumn('activitat', Activitat.activitat, 'activitat'),
             AlchemicalTableModelColumn('adreca', Carrerer.etiqueta, 'ubicacio.carrerer.etiqueta'),
             AlchemicalTableModelColumn('numero', Ubicacio.numero, 'ubicacio.numero'),
             AlchemicalTableModelColumn('nucli', Ubicacio.nucli, 'ubicacio.nucli')]
        )
        self.show_print_dialog('Activitats que no estan en actiu', inactivesModel, ImpressioInactives)


class FilterOption:
    def __init__(self, name, model, filter):
        self.name = name
        self.model = model
        self.filter = filter
        self.old_value = None
