from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog

from ui.compiled.Ui_RegistreGeneral import Ui_RegistreGeneral


class RegistreGeneral(QDialog, Ui_RegistreGeneral):
    def __init__(self, parent):
        super(RegistreGeneral, self).__init__(parent, flags=Qt.Window)

        self.setupUi(self)

        # TODO: Arreglar el vertical resize
