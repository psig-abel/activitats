# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:\PSIG\Projectes\CTBB\Activitats\resources\ui\Imprimeix.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Imprimeix(object):
    def setupUi(self, Imprimeix):
        Imprimeix.setObjectName("Imprimeix")
        Imprimeix.resize(478, 557)
        self.verticalLayout = QtWidgets.QVBoxLayout(Imprimeix)
        self.verticalLayout.setObjectName("verticalLayout")
        self.titol = QtWidgets.QLabel(Imprimeix)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.titol.setFont(font)
        self.titol.setObjectName("titol")
        self.verticalLayout.addWidget(self.titol)
        self.subtitol = QtWidgets.QLabel(Imprimeix)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.subtitol.setFont(font)
        self.subtitol.setObjectName("subtitol")
        self.verticalLayout.addWidget(self.subtitol)
        self.scrollArea = QtWidgets.QScrollArea(Imprimeix)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.scrollArea.setFrameShadow(QtWidgets.QFrame.Plain)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.llista = QtWidgets.QWidget()
        self.llista.setGeometry(QtCore.QRect(0, 0, 458, 464))
        self.llista.setStyleSheet("QWidget { background-color: #fff; }")
        self.llista.setObjectName("llista")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.llista)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.scrollArea.setWidget(self.llista)
        self.verticalLayout.addWidget(self.scrollArea)
        self.printButton = QtWidgets.QPushButton(Imprimeix)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.printButton.sizePolicy().hasHeightForWidth())
        self.printButton.setSizePolicy(sizePolicy)
        self.printButton.setAutoDefault(False)
        self.printButton.setObjectName("printButton")
        self.verticalLayout.addWidget(self.printButton, 0, QtCore.Qt.AlignHCenter)

        self.retranslateUi(Imprimeix)
        QtCore.QMetaObject.connectSlotsByName(Imprimeix)

    def retranslateUi(self, Imprimeix):
        _translate = QtCore.QCoreApplication.translate
        Imprimeix.setWindowTitle(_translate("Imprimeix", "Llistat de comunicacions d\'activitats/empreses"))
        self.titol.setText(_translate("Imprimeix", "REGISTRE MUNICIPAL D\'ACTIVITATS"))
        self.subtitol.setText(_translate("Imprimeix", "(Segons l\'article tal)"))
        self.printButton.setText(_translate("Imprimeix", "Imprimeix..."))

