# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:\PSIG\Projectes\CTBB\Activitats\resources\ui\MainWindow.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(828, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.filtraGroupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.filtraGroupBox.setCheckable(True)
        self.filtraGroupBox.setChecked(False)
        self.filtraGroupBox.setObjectName("filtraGroupBox")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.filtraGroupBox)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout.addWidget(self.filtraGroupBox)
        self.activitatsView = QtWidgets.QTableView(self.centralwidget)
        self.activitatsView.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.activitatsView.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.activitatsView.setShowGrid(False)
        self.activitatsView.setSortingEnabled(True)
        self.activitatsView.setObjectName("activitatsView")
        self.activitatsView.horizontalHeader().setCascadingSectionResizes(True)
        self.activitatsView.horizontalHeader().setHighlightSections(False)
        self.activitatsView.horizontalHeader().setStretchLastSection(False)
        self.verticalLayout.addWidget(self.activitatsView)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.addButton = QtWidgets.QPushButton(self.centralwidget)
        self.addButton.setObjectName("addButton")
        self.horizontalLayout.addWidget(self.addButton)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.editGeneralButton = QtWidgets.QPushButton(self.centralwidget)
        self.editGeneralButton.setObjectName("editGeneralButton")
        self.horizontalLayout.addWidget(self.editGeneralButton)
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.horizontalLayout.addWidget(self.pushButton_3)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.printActivitatsButton = QtWidgets.QPushButton(self.groupBox)
        self.printActivitatsButton.setObjectName("printActivitatsButton")
        self.horizontalLayout_2.addWidget(self.printActivitatsButton)
        self.printReparButton = QtWidgets.QPushButton(self.groupBox)
        self.printReparButton.setObjectName("printReparButton")
        self.horizontalLayout_2.addWidget(self.printReparButton)
        self.printInactivesButton = QtWidgets.QPushButton(self.groupBox)
        self.printInactivesButton.setObjectName("printInactivesButton")
        self.horizontalLayout_2.addWidget(self.printInactivesButton)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.verticalLayout.addWidget(self.groupBox)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 828, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Activitats"))
        self.filtraGroupBox.setTitle(_translate("MainWindow", "Filtra"))
        self.addButton.setText(_translate("MainWindow", "&Afegeix..."))
        self.editGeneralButton.setText(_translate("MainWindow", "&Edita dades generals..."))
        self.pushButton_3.setText(_translate("MainWindow", "Edita serveis &territorials..."))
        self.groupBox.setTitle(_translate("MainWindow", "Impressions"))
        self.printActivitatsButton.setText(_translate("MainWindow", "Registre municipal d\'activitats..."))
        self.printReparButton.setText(_translate("MainWindow", "Registre municipal REPAR..."))
        self.printInactivesButton.setText(_translate("MainWindow", "Activitats que no estan en actiu..."))

